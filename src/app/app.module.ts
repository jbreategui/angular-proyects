import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { SharedModule } from './shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// router van en los imports
import { APP_ROUTES } from './route';
import { ServiceModule } from './services/service.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingScreenInterceptor } from './helpers/loading.interceptor';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/es-PE';
// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'PE');
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PagesComponent,
    LoadingScreenComponent,
    
  ],
  imports: [
    BrowserModule,
    SharedModule,
    APP_ROUTES,
    ServiceModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule


  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: LoadingScreenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
