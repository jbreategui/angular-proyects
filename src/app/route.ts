import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './login/login.component';


const appRoutes: Routes = [

    { path: 'login', component: LoginComponent },
    {
        path: 'pages',
        component: PagesComponent,
        loadChildren: './pages/pages.module#PagesModule'
    },
    { path: '**', redirectTo: '/login', pathMatch: 'full' },
];


export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
