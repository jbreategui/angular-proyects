import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PEDIDO_ROUTES } from './pedido.route';
import { ListaPedidoComponent } from './lista-pedido.component';


@NgModule({
  declarations: [
  ListaPedidoComponent
],
  imports: [
    CommonModule,
    PEDIDO_ROUTES,



  ]
})
export class PedidoModule { }
