import { RouterModule, Routes } from '@angular/router';
import { PedidoComponent } from './pedido/pedido.component';




const pagesRoutes: Routes = [

    { path: 'pedido', component: PedidoComponent, loadChildren: './pedido/pedido.module#PedidoModule' },
    { path: '', redirectTo: '/pages/pedido', pathMatch: 'full' },
];


export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
